#include "Virtual_FS.h"
#include "Disk_Emulator.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#define FS_MAGIC           0xf0f03410 // ???
#define INODES_PER_BLOCK   128 // Dfinition that each block will have 128 inodes
#define POINTERS_PER_INODE 5 // Dfinition of the number of ponters per inode
#define POINTERS_PER_BLOCK 1024 // Dfinition of the number of ponters per block
#define BLOCK_SIZE 4096 // Dfinition of the number of size block
#define FREE 0 // not active
#define TAKEN 1 // active



int * bitmap = NULL; 

int built = 0;
int copysize;

struct fs_superblock { // function to create the struct in superblock
	int magic; // the magic number
	int nblocks; // the munber of block
	int ninodeblocks; // the number of inodes in the blocks 
	int ninodes; // the number of inodes
};

struct fs_inode { // function to create the struct in inode
	int isvalid; // Active or not 
	int size; // the size of inode
	int direct[POINTERS_PER_INODE]; // the 5 inodes thet are in the block 
	int indirect; // the pointer to another direct (if any)
};

union fs_block { // In union, all members share the same memory location
	struct fs_superblock super;
	struct fs_inode inode[INODES_PER_BLOCK];
	int pointers[POINTERS_PER_BLOCK];
	char data[DISK_BLOCK_SIZE];
};


// an attempt to format an already-mounted disk should do nothing and return failure
int fs_format() // The function goes into the super block and arranges it and then it goes into the blocks and into the anodes and resets them
{
	
	if(bitmap != NULL){ //return fail if already mounted
		printf("disk is already mounted\n");
		return 0;
	}
	int nblocks = disk_size(); // the function disk_size given me the number of blocks
	
	// --clear all the data existed in blocks--

	// --initialize super block--
	union fs_block data;
	data.super.nblocks = nblocks; // set nblocks
	int inodesblocks = (int)(nblocks*0.1) + ((nblocks%10 == 0) ? 0 : 1); // set ninode block. Multiply all inodes by 1.0 and then if the remainder of 10% = 0 do nothing and if you do not add one block.
	data.super.ninodeblocks = inodesblocks; // I push it to class
	data.super.ninodes = inodesblocks * INODES_PER_BLOCK; // At a size of 128
	data.super.magic = FS_MAGIC; //  0xf0f03410
	//printf("in format: ninodesblocks: %d ninodes: %d\n",data.super.ninodeblocks, data.super.ninodes); ??
	disk_write(0, data.data); // Write the information from cell 0 

	for(int i = 1; i < inodesblocks; i++){ // A loop that passes block by block
		union fs_block block;
		for(int j = 0; j < INODES_PER_BLOCK; j++){ // Another loop that passes through the inode
			block.inode[j].isvalid = 0; // clear all the pointer(direct & indrect)
			block.inode[j].size = 0; // clear all the pointer(direct & indrect)
			memset(block.inode[j].direct, 0, POINTERS_PER_INODE * 4); // memset() is used to fill a block of memory with a particular value and accept to variables, the first is Number of bytes to be filled starting and the secned is the bites.
			block.inode[j].indirect = 0; // Resets everything
		}
		disk_write(i, block.data); // Write the information from cell 0 
	}
	return 1;
}

void fs_debug() // --Scan a mounted filesystem and report on how the inodes and blocks are organized--
{
	union fs_block block; // In union, all members share the same memory location

	disk_read(0,block.data); // Read the information starting from cell 0

	printf("superblock:\n"); 
	if (block.super.magic == FS_MAGIC){ // If the magic number is equal to the number
		printf("    magic number is valid\n");
	}
	else { // If the magic number is not equal to the number
		printf("    magic number is not valid\n");
	}
	printf("    %d blocks on disk\n",block.super.nblocks); // Print the nblocks
	printf("    %d blocks for inodes\n",block.super.ninodeblocks); // Print the ninodeblocks
	printf("    %d inodes total\n",block.super.ninodes); // Print the ninodes

	int ninodeblocks = block.super.ninodeblocks; // Take the ninodeblocks from block.super.ninodeblocks
	if (ninodeblocks < 0){return;} // If it is less than 0 return
	for (int i = 1; i< ninodeblocks; i++){ // each inode block
		disk_read(i,block.data); // Read the information starting from cell 1
		for (int j = 0; j < INODES_PER_BLOCK; j++){ // each inode
			struct fs_inode inode = block.inode[j]; // Take the fs_inode inode from block.inode[j]
			if (!inode.isvalid){ // If it is not isvalid
				//printf("inode.isvalid %d\n", inode.isvalid);
				continue; 
			}
			printf("inode %d:\n", j+i*INODES_PER_BLOCK + 1); 
			printf("    size: %d bytes\n",fs_getsize(j+i*INODES_PER_BLOCK + 1));
			printf("    direct blocks: ");
			for (int k = 0;k<POINTERS_PER_INODE; k++){ // each pointedblock
				int pointedblock = inode.direct[k];
				if (pointedblock != 0){ // If it is not equal to 0
					printf("%d ", pointedblock); // print the pointedblock
				}
			}
			printf("\n");

			if (!inode.indirect){continue;} // 
			printf("    indirect block: %d\n", inode.indirect);
			printf("    indirect data blocks: "); 
			union fs_block indirectblock;
			disk_read(inode.indirect, indirectblock.data); // Read the information of the direct
			for (int l = 0; l < POINTERS_PER_BLOCK; l++){ // each direct
                        	if (indirectblock.pointers[l]!=0){ // If it is not equal to 0
					printf("%d ",indirectblock.pointers[l]); // print the indirectblock.pointers
				}
			
			}
			printf("\n");	
		}
	}
}

int fs_mount() // build a new free block bitmap
{
	if(bitmap != NULL){ // if the bitmap not it is equal to NULL
		printf("It has already been mounted!\n"); // That means it has already been installed
		return 0;
	}
	union fs_block block; // if bitmap = NULL
	disk_read(0,block.data); // Read the information starting from cell 0
	int ninodeblocks = block.super.ninodeblocks; // Take the ninodeblocks from block.super.ninodeblocks
	int nblocks = block.super.nblocks; // Take the nblocks from block.super.nblocks
	int i,j,k; // definde variaboles i,j,k
	int fileblocks; // file_size/block_size
	struct fs_inode inode; // call the function fs_inode named inode
	union fs_block datablock; // call the function fs_block named datablock
	bitmap = (int *)malloc(nblocks); // Indicates memory allocation to nblocks
	memset(bitmap,0,nblocks); // Sequences everything in zeros
	bitmap[0] = TAKEN; 
	for(i = 1; i <= ninodeblocks; i++){ // Go through each ninodeblocks and write down TAKEN
		bitmap[i] = TAKEN;
		disk_read(i,block.data); // Write
		for(j = 0; j < INODES_PER_BLOCK; j++){  // Go through each INODES_PER_BLOCK
			inode = block.inode[j]; // Per inode
			if(inode.isvalid){ // if it is isvalid
				fileblocks = inode.size / BLOCK_SIZE; // Enter the size to BLOCK_SIZE
				if(inode.size % BLOCK_SIZE){ // per % BLOCK_SIZE
					fileblocks++;
				}
				for(k = 0; k < POINTERS_PER_INODE; k++){ // Go through each POINTERS_PER_INODE < k
					bitmap[inode.direct[k]] = TAKEN; // Write to direct the TAKEN (1)
				}
				if(fileblocks > POINTERS_PER_INODE){ // if fileblocks > POINTERS_PER_INODE we need more space
					disk_read(inode.indirect,datablock.data); // Write
					for(k = 0; k < (fileblocks - POINTERS_PER_INODE); k++){ // Write all voters as TAKEN
						bitmap[datablock.pointers[k]] = TAKEN;
					}
				}
			}
		}
	}
	return 1;
}

int fs_create() 
{
	if(bitmap == NULL){ // This means that the disk was not mounted so we will mounted it
		printf("The disk haven't been mounted!\n");
		return -1;
	}
	union fs_block block; 	// search through the top 10% blocks finding the first avalible inode 
	disk_read(0, block.data); // read
	for(int i = 0; i < block.super.ninodeblocks; i++){ // Go through all the anodes
		union fs_block tempblock; 
		disk_read(i+1, tempblock.data); // read
		for(int j = 0; j < INODES_PER_BLOCK; j++){ // per INODES_PER_BLOCK
			if(tempblock.inode[j].isvalid == 0){ // if write 0
				tempblock.inode[j].isvalid = 1; // Change to 1
				tempblock.inode[j].size = 0; // And set size 0
				disk_write(i+1, tempblock.data); // Write all the information inside
				disk_write(0, block.data);
				printf("create with an inumber of : %d", i*INODES_PER_BLOCK + j + 1);
				return i * INODES_PER_BLOCK + j + 1; // Give me back the INODES_PER_BLOCK number you created
			}
		}
	}
	return -1;
}

int fs_delete(int inumber) // Deleting inodes
{
	if(bitmap == NULL){ // If it's worth it means the disk is not monted
		printf("The disk haven't been mounted!\n");
		return 0;
	}
	union fs_block superblock; 	// --check if the input inumber if valid (Extreme case)--
	disk_read(0, superblock.data);	
	if(inumber > superblock.super.ninodes || inumber == 0){
		printf("The inumber is invalid!\n");
		return 0;
	}

	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1; // Find the location
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	union fs_block block;	
	disk_read(blocknum, block.data); // After you have found read the date from it
	struct fs_inode inode = block.inode[inodenum];

	if(inode.isvalid){ // If inode.isvalid
		int fileblocks = inode.size/BLOCK_SIZE + ((inode.size%BLOCK_SIZE == 0)?0:1); // Find the location
		for(int i = 0; i < POINTERS_PER_INODE; i++){
			if(inode.direct[i] == 0) // If it is equal to 0
				continue;
			bitmap[inode.direct[i]] = FREE; // If not, insert FREE (0)
		}
		if(fileblocks > POINTERS_PER_INODE){ // --Voter reset--
			union fs_block datablock;
			disk_read(inode.indirect, datablock.data);
			for(int k = 0; k < (fileblocks - POINTERS_PER_INODE); k++){
				if(inode.direct[k] == 0)
					continue;
				bitmap[datablock.pointers[k]] = FREE;
			}
		}
		block.inode[inodenum].isvalid = 0; // --Reset in all places previously registered up to the top--
		block.inode[inodenum].size = 0;
		memset(block.inode[inodenum].direct, 0, POINTERS_PER_INODE * 4);
		block.inode[inodenum].indirect = 0;
		disk_write(blocknum, block.data);
		disk_write(0, superblock.data);
	}
	return 1; // Give back success
}


int fs_getsize( int inumber ) // get size
{
	union fs_block superblock; /// --check if the input inumber if valid (Extreme case)--
	disk_read(0, superblock.data);	
	if(inumber > superblock.super.ninodes || inumber == 0){
		printf("The inumber is invalid!\n");
		return 0;
	}

	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1; // Find the location
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	union fs_block block;
	disk_read(blocknum, block.data); // read
	struct fs_inode inode = block.inode[inodenum];
	if(inode.isvalid == 0){ // if not exists
		printf("inumber is not valid. Not create yet.\n");
		return -1;
	}	
	//printf("%d\n", inode.size);
	return inode.size; // rturen the size

}


int fs_read( int inumber, char *data, int length, int offset ) // the punction get 4 variabol. The first is the length of the anodes in the file, the second is date, The third is the size of the buffer, The fourth is how much I have already transferred.
{	
	if(bitmap == NULL){ // If it's worth it means the disk is not monted
		printf("The disk haven't been mounted!\n");
		return -1;
	}
	union fs_block superblock; // --check if the input inumber if valid (Extreme case)--
	disk_read(0, superblock.data);	
	if(inumber > superblock.super.ninodes || inumber == 0){
		printf("The inumber is invalid!\n");
		return 0;
	}
	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1; // Find the location
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	union fs_block block;

	disk_read(0,block.data);
	if(block.super.magic == FS_MAGIC){ // Checks if the magic number is correct
		union fs_block block;
		disk_read(blocknum, block.data);
		struct fs_inode inode = block.inode[inodenum];
		if(inode.isvalid){ // check if input is valid
			if(inode.size < offset) 
				return 0;
			int copysize = (inode.size - offset  < length) ? inode.size - offset : length; // Take the smallest
			if(copysize > 0){
				union fs_block indirect; // --Resize the block anywhere--
				disk_read(inode.indirect, indirect.data);
				int blockbegin = offset / BLOCK_SIZE;
				int blockoffset = offset % BLOCK_SIZE;
				int datablocknum = (copysize > (BLOCK_SIZE - offset)) ? (copysize - (BLOCK_SIZE - blockoffset)) / BLOCK_SIZE : 0;
				int first_length = (copysize > BLOCK_SIZE - blockoffset) ? BLOCK_SIZE - blockoffset : copysize;	
				int last = (copysize > first_length)?(copysize - first_length) % BLOCK_SIZE : 0;

				//copy the first number
				//printf("length %d \t offset %d \t copysize %d, \tfirst length %d \t last %d\n", length, offset, copysize, first_length, last);
				union fs_block datablock; // If it is smaller than it you will read the date
				if(blockbegin < POINTERS_PER_INODE){
					disk_read(inode.direct[blockbegin], datablock.data);
				}
				else{
					int tempblocknum = indirect.pointers[blockbegin - POINTERS_PER_INODE]; // If you do not go to the Indus and call from there
					disk_read(tempblocknum, datablock.data);
				}
				memcpy(data, datablock.data, first_length); // memcpy Override the information
			
				//printf("first block complete, datablock number:%d\n", datablocknum);
				for(int i = 1; i <= datablocknum; i++){ // --copy rest blocks--
					union fs_block tempblock;
					if(blockbegin + i < POINTERS_PER_INODE){
						disk_read(inode.direct[blockbegin + i], tempblock.data);
					}else{
						int tempblocknum = indirect.pointers[blockbegin + i - POINTERS_PER_INODE];
						disk_read(tempblocknum, tempblock.data);
					}
					memcpy(data + first_length + BLOCK_SIZE * (i-1), tempblock.data, BLOCK_SIZE); // Overrides existing date
				}

				if(last != 0){ 
					//printf("copy last block\n");
					union fs_block tempblock;
					if(blockbegin + datablocknum + 1 < POINTERS_PER_INODE){
						disk_read(inode.direct[blockbegin + datablocknum + 1], tempblock.data);
					}else{
						int tempblocknum = indirect.pointers[blockbegin + datablocknum + 1 - POINTERS_PER_INODE];
						disk_read(tempblocknum, tempblock.data);
					}
					memcpy(data + first_length + BLOCK_SIZE * datablocknum, tempblock.data, BLOCK_SIZE);
					//printf("last block copy complete\n");
				}
			}
			return copysize;
		}
	}
	return 0;
}

int findFree(){ // Find empty seats
	if(bitmap == NULL){ // If it's worth it means the disk is not monted
		printf("The disk haven't been mounted!\n");
		return -1;
	}
	union fs_block block; // Go through all the anodes
	disk_read(0, block.data);
	int nblocks = block.super.nblocks;
	int inodesblocks = block.super.ninodeblocks;
	for(int i = inodesblocks; i < nblocks; i++){
		if(bitmap[i] == FREE){ // If i empty
			return i; // Return it
		}
	}
	return -1;
}


int fs_write( int inumber, const char *data, int length, int offset ) //  // the punction get 4 variabol. The first is the length of the anodes in the file, the second is date, The third is the size of the buffer, The fourth is how much I have already transferred.
{
	printf("enter write\n"); 
	printf("length %d, /t offset %d\n", length, offset);
	if(bitmap == NULL){ // If it's worth it means the disk is not monted
		printf("The disk haven't been mounted!\n");
		return -1;
	}
	union fs_block superblock; // --check if the input inumber if valid (Extreme case)--
	disk_read(0, superblock.data);	
	if(inumber > superblock.super.ninodes || inumber == 0){
		printf("The inumber is invalid!\n");
		return 0;
	}
	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1; // Find the location
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	union fs_block block;
	int ret = 0;

	disk_read(0,block.data); 
	if(block.super.magic == FS_MAGIC){ // Checks if the magic number is correct
		union fs_block block;
		disk_read(blocknum, block.data); 
		struct fs_inode inode = block.inode[inodenum]; // check if input is valid
		if(inode.isvalid){ //check the input
			if(inode.size < offset)
				return ret;

			int blockbegin = offset / BLOCK_SIZE; // --Resize the block anywhere--
			int blockoffset = offset % BLOCK_SIZE;
			int datablocknum = (length > (BLOCK_SIZE - offset))?(length - (BLOCK_SIZE - blockoffset)) / BLOCK_SIZE : 0;
			int first_length = (length > BLOCK_SIZE - blockoffset) ? BLOCK_SIZE - blockoffset : length;	
			int last = (first_length < length)?(length - first_length) % BLOCK_SIZE : 0;
	
			int directblocknum = datablocknum + blockbegin + 1; // Go through all the directblocknum
			for(int i = 0; i < directblocknum; i++){
				if(inode.direct[i] != 0) // If it is not equal to 0
					continue;
				int freeblock = findFree(); // --If so use the findFree function and reset everything--
				if(freeblock != -1){
					bitmap[freeblock] = TAKEN;
					inode.direct[i] = freeblock;
				}
			}
			
			int extrablock = datablocknum + blockbegin + 1 - POINTERS_PER_INODE; // If it's equal then need more space

			if(extrablock > 0 && inode.indirect == 0){ // --allocate indirect pointer--
				int freeblock = findFree();
				if(freeblock != -1){
					bitmap[freeblock] = TAKEN;
					inode.indirect = freeblock;
				}
			}
			if(extrablock > 0){
				union fs_block indirect;
				disk_read(inode.indirect, indirect.data); // allocate space for data
				for(int k = 0; k < extrablock; k++){
					if(indirect.pointers[k] != 0)
						continue;
					int tempfree = findFree();
					if(tempfree != -1){
						bitmap[tempfree] = TAKEN;
						indirect.pointers[k] = tempfree;
					}else{
						indirect.pointers[k] = 0;
					}
				}
				disk_write(inode.indirect, indirect.data);
			}
			
			block.inode[inodenum] = inode;
			disk_write(blocknum, block.data); // Write the date

			union fs_block indirect; // write the first block
			disk_read(inode.indirect, indirect.data);
			union fs_block datablock;
			int tempblocknum; // Save all voters
			if(blockbegin > POINTERS_PER_INODE){
				tempblocknum = indirect.pointers[blockbegin - POINTERS_PER_INODE];
			}else{
				tempblocknum = inode.direct[blockbegin];
			}

			if(tempblocknum == 0){ // Check if the tempblocknum is equal to 0
				block.inode[inodenum].size += ret; // You'll find his location
				disk_write(blocknum, block.data); // Write the date
				return ret;
			}
			
			disk_read(tempblocknum, datablock.data); // the fuction accepts 2 variabole, 1. location 2. date
			memcpy(datablock.data + blockoffset, data, first_length); // Overrides existing date
			disk_write(inode.direct[blockbegin], datablock.data);
			ret += first_length;

			for(int i = 1; i <= datablocknum; i++){	 // write the rest block
				if(blockbegin + i < POINTERS_PER_INODE){ // That is, the place is over
					if(inode.direct[blockbegin + i] == 0){ // find his location
						block.inode[inodenum].size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_write(inode.direct[blockbegin + i], data+first_length + BLOCK_SIZE*(i-1)); // Write the size
				}else{
					int tempblocknum = indirect.pointers[blockbegin + i - POINTERS_PER_INODE]; // Will create new places
					if(tempblocknum == 0){
						block.inode[inodenum].size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_write(tempblocknum, data + first_length + BLOCK_SIZE*(i-1));
				}
				ret += BLOCK_SIZE;
			}
			
			if(last != 0){ // If the buffer is different from zero
				union fs_block tempblock;
				
				if(blockbegin + datablocknum < POINTERS_PER_INODE){ // More pointers need to be added
					if(inode.direct[blockbegin + datablocknum] == 0){ // Check that the storage is empty
						block.inode[inodenum].size += ret; 
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_read(inode.direct[blockbegin+datablocknum], tempblock.data); // read By the last location
					memcpy(tempblock.data, data + first_length + BLOCK_SIZE * datablocknum, last);
					disk_write(inode.direct[blockbegin + datablocknum], tempblock.data); // Write
				}else{
					int tempblocknum = indirect.pointers[blockbegin + datablocknum - POINTERS_PER_INODE]; // Looking for it by location
					if(tempblocknum == 0){
						block.inode[inodenum].size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					memcpy(tempblock.data, data + first_length + BLOCK_SIZE * datablocknum, last); // Overrides existing date
					disk_write(tempblocknum, tempblock.data);
				}
				ret += last;
				
			}
			block.inode[inodenum].size += ret;
			disk_write(blocknum, block.data);

		}
	}
	return ret;
}
