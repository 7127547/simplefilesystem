#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "Disk_Emulator.h"

#define DISK_MAGIC 0xdeadbeef // ??? 

static FILE *diskfile; // statik is the statik variable 
static int nblocks=0; // given me the number of blocks
static int nreads=0; // given me the number of reads
static int nwrites=0; // given me the number of writes

int disk_init( const char *filename, int n ) // A function that writes to a disk and gets two arguments, the first points with the name, and the second the block number 
{
	diskfile = fopen(filename,"r+"); // opne the file
	if(!diskfile) diskfile = fopen(filename,"w+"); // if there is no file it produces one
	if(!diskfile) return 0; // Extreme case! If unsuccessful returns 0 

	ftruncate(fileno(diskfile),n*DISK_BLOCK_SIZE); // The function that flexes the size of the file. Gets two arguments, the first is where there is no information, and the second is up to where there is no information 

	nblocks = n;
	nreads = 0;
	nwrites = 0;

	return 1;
}

int disk_size() // the function disk_size given me the number of blocks
{
	return nblocks;
}

static void sanity_check( int blocknum, const void *data ) // a function that checks an end case and accept the number of blocks and the pointer of data 
{
	if(blocknum<0) { // if blocknum<0
		printf("ERROR: blocknum (%d) is negative!\n",blocknum); // print ERROR: blocknum (%d) is negative! and the blocknum
		abort();
	}

	if(blocknum>=nblocks) { // if blocknum>=nblocks
		printf("ERROR: blocknum (%d) is too big!\n",blocknum); // print ERROR: blocknum (%d) is too big! and the blocknum 
		abort();
	}

	if(!data) { // If there is no date
		printf("ERROR: null data pointer!\n"); // print RROR: null data pointer!
		abort();
	}
}

void disk_read( int blocknum, char *data ) // void is function the we not need return, disk_read is function the read, and accept the number of blocks and the pointer of data 
{
	sanity_check(blocknum,data); // sanity_check this is a tast that the information is correct, and accept the number of blocks and the pointer of data 

	fseek(diskfile,blocknum*DISK_BLOCK_SIZE,SEEK_SET); // fseek is used to move file pointer asscoiated with a given file to a specific position, and accect three variables, the first indicates the current location of the file pointer (diskfile), the second indicates the end of the file (blocknum*DISK_BLOCK_SIZE), and the third indicates the beginning of the file (SEEK_SET) 

	if(fread(data,DISK_BLOCK_SIZE,1,diskfile)==1) { // the fread reads the block of date from the stream, and accept four variables, the first is buffer it specifies the pointer to the block of memory with a size of at least (size*count) bytes to store the objects (date), the second is size it specifies the size of each object in bytes. size_t is an unsigned integral type (DISK_BLOCK_SIZE), and the third is count it specifies the number of elements, each one with a size of size bytes (1). and the fourth stream it specifies the file stream to read the data from (diskfile). 
		nreads++; // given me the number of reads
	} else {
		printf("ERROR: couldn't access simulated disk: %s\n",strerror(errno)); // returns pointer to error
		abort();
	}
}

void disk_write( int blocknum, const char *data ) // void is function the we not need return, disk_write is function the read, and accept the number of blocks and the pointer of data 
{
	sanity_check(blocknum,data); // sanity_check this is a tast that the information is correct, and accept the number of blocks and the pointer of data 

	fseek(diskfile,blocknum*DISK_BLOCK_SIZE,SEEK_SET); // fseek is used to move file pointer asscoiated with a given file to a specific position, and accect three variables, the first indicates the current location of the file pointer (diskfile), the second indicates the end of the file (blocknum*DISK_BLOCK_SIZE), and the third indicates the beginning of the file (SEEK_SET) 

	if(fwrite(data,DISK_BLOCK_SIZE,1,diskfile)==1) { // the fread reads the block of date from the stream, and accept four variables, the first is buffer it specifies the pointer to the block of memory with a size of at least (size*count) bytes to store the objects (date), the second is size it specifies the size of each object in bytes. size_t is an unsigned integral type (DISK_BLOCK_SIZE), and the third is count it specifies the number of elements, each one with a size of size bytes (1). and the fourth stream it specifies the file stream to read the data from (diskfile). 
		nwrites++; // given me the number of writes
	} else {
		printf("ERROR: couldn't access simulated disk: %s\n",strerror(errno)); // returns pointer to error 
		abort();
	}
}

void disk_close() // void is function the we not need return,
{
	if(diskfile) { // if diskfile raturn 1
		printf("%d disk block reads\n",nreads); // print disk block reads and the number of reads
		printf("%d disk block writes\n",nwrites); // print disk block writes and the number of writes
		fclose(diskfile); // and reset it back to 0
		diskfile = 0;
	}
}
