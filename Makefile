GCC=/usr/bin/gcc

simplefs: shell.o Virtual_FS.o Disk_Emulator.o
	$(GCC) shell.o Virtual_FS.o Disk_Emulator.o -o simplefs

shell.o: shell.c
	$(GCC) -Wall shell.c -c -o shell.o -g

Virtual_FS.o: Virtual_FS.c Virtual_FS.h
	$(GCC) -Wall Virtual_FS.c -c -o Virtual_FS.o -g

Disk_Emulator.o: Disk_Emulator.c Disk_Emulator.h
	$(GCC) -Wall Disk_Emulator.c -c -o Disk_Emulator.o -g

clean:
	rm simplefs Disk_Emulator.o Virtual_FS.o shell.o